###
# Adds an executable with all flags and creates symbolic file links
# to grids and input files
# ATTENTION: symbolic links are not supported by all file systems, e. g.
# it will not work on Windows.
#
# Arguments:
# - dumux_test_executable:        name of the executable
# - dumux_test_executable_source: name of the source file
###
macro(add_executable_all dumux_test_executable dumux_test_executable_source)
  # if present, add file/folder link
  add_folder_link(grids)
  add_file_link(${dumux_test_executable}.input)
  add_file_link(${dumux_test_executable}_reference.input)
  add_executable(${dumux_test_executable} EXCLUDE_FROM_ALL ${dumux_test_executable_source})
  add_dumux_all_flags(${dumux_test_executable})
endmacro()
