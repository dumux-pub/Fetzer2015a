Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

T. Fetzer, K. Smits, and R. Helmig<br>
[Effect of Turbulence and Roughness on Coupled Porous-Medium/Free Flow Exchange Processes]
(http://link.springer.com/article/10.1007/s11242-016-0654-6)<br>
Transport in Porous Media, 2016<br>
DOI: 10.1007/s11242-016-0654-6

You can use the .bib file provided [here](Fetzer2015a.bib).


Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installFetzer2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2015a/raw/master/installFetzer2015a.sh)
in this folder.

```bash
mkdir -p Fetzer2015a && cd Fetzer2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2015a/raw/master/installFetzer2015a.sh
sh ./installFetzer2015a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.

Applications
============

The applications used for this publication can be found in appl/multidomain/windTunnel.
This folder also contains the jobfiles which specify the individual setups.
The are tests which can be run using the ctest facility to check whether the reference
results are obtained.

* __test_windTunnelTopOpen__:
  The basic setup of the wind tunnel with the top of the free flow
  section as a symmetry/Dirichlet at the centerline.
* __test_windTunnelTopClosed__:
  The setup of the wind tunnel with the top of the free flow  as a wall.
* __test_windTunnelTopOpenSomerton__:
  Same setup as test_windTunnelTopOpen with the difference that the Somerton law is used
  for the effective thermal conductivity.

In order to run an executable, type e.g.:
```bash
cd dumux-Fetzer2015a/build-cmake/appl/multidomain/windTunnel/
make
./test_windTunnelTopOpen
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installFetzer2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2015a/raw/master/installFetzer2015a.sh).

In addition the following external software packages, with one of the two linear
solvers, are necessary for compiling the executables:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              | 3.5.2   | build tool        |
| suitesparse        | 4.4.6   | matrix algorithms |
| SuperLU            | 4.3     | linear solver     |
| UMFPack            | 5.7.1   | linear solver     |
| ug                 | 3.11.0  | grid manager      |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ | 3.5     |
| gcc/g++       | 4.7     |
