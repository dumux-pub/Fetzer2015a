// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the coupled non-isothermal two-component ZeroEq and
 *        non-isothermal two-phase two-component Darcy model using a
 *        pipe flow which is symmetrized at the center line.
 */

#include "config.h"
#include <iostream>

#define TOP_OPEN 1

#include <dune/common/parallel/mpihelper.hh>

#if HAVE_DUNE_MULTIDOMAIN

#include <dumux/common/start.hh>

#include "2cnizeroeq2p2cniproblem.hh"

/*!
 * \brief Print a usage string for simulations.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void printUsage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of optional options for this program is:\n"
                           "[ParameterGroup]\n"
                           "Parameter           ParameterDescription\n"
                           "\n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv)
{
#if (HAVE_SUPERLU || HAVE_UMFPACK)
    typedef TTAG(TwoCNIZeroEqTwoPTwoCNIProblem) ProblemTypeTag;
    return Dumux::start<ProblemTypeTag>(argc, argv, printUsage);
#else
#warning "You need to have SuperLU or UMFPack installed to run this test."
    std::cerr << "You need to have SuperLU or UMFPack installed to run this test\n";
    return 77;
#endif
}

#else
int main(int argc, char** argv)
{
#warning You need to have dune-multidomain installed to run this test
    std::cerr << "You need to have dune-multidomain installed to run this test\n";
    return 77;
}
#endif
