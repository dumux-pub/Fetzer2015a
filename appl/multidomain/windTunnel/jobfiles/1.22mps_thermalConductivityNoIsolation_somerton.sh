#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/1.22mps/thermalConductivityNoIsolation/somerton
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpenSomerton $simdir
cp $source/executables/davarzani2014_plain_1.22mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpenSomerton davarzani2014_plain_1.22mps.input \
  -PorousMedium.IsolatedPorousMediumBox false \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
