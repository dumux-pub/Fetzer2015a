#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/1.22mps/roughnessBL/10mmY+30
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpen $simdir
cp $source/executables/davarzani2014_plain_1.22mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpen davarzani2014_plain_1.22mps.input \
  -BoundaryLayer.Model 5 \
  -BoundaryLayer.Offset 0 \
  -BoundaryLayer.YPlus 30 \
  -BoundaryLayer.RoughnessLength 10e-3 \
  -MassTransfer.Model 0 \
  -ZeroEq.EddyViscosityModel 0 \
  -ZeroEq.EddyDiffusivityModel 0 \
  -ZeroEq.EddyConductivityModel 0 \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
