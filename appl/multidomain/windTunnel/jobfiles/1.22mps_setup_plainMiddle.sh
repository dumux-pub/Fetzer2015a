#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/1.22mps/setup/plainMiddle
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpen $simdir
cp $source/executables/davarzani2014_plain_1.22mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpen davarzani2014_plain_1.22mps.input \
  -Grid.UpperRightX 0.70 \
  -Grid.NumberOfCellsX 56 \
  -Grid.NoDarcyX1 0.25 \
  -Grid.NoDarcyX2 0.50 \
  -Grid.RunUpDistanceX1 0.251 \
  -Grid.RunUpDistanceX2 0.499 \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
