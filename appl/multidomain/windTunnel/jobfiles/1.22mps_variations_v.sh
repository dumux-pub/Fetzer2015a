#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/1.22mps/variations/v
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpen $simdir
cp $source/executables/davarzani2014_plain_1.22mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpen davarzani2014_plain_1.22mps.input \
  -FreeFlow.VelDataFile /home/lh2/fetzer/results/windtunnel/davarzani2014/1.22mps_windSpeed.dat \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
