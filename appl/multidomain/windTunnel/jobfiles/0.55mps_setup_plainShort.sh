#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/0.55mps/setup/plainShort
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpen $simdir
cp $source/executables/davarzani2014_plain_0.55mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpen davarzani2014_plain_0.55mps.input \
  -Grid.UpperRightX 0.50 \
  -Grid.NumberOfCellsX 40 \
  -Grid.NoDarcyX1 0.05 \
  -Grid.NoDarcyX2 0.30 \
  -Grid.RunUpDistanceX1 0.051 \
  -Grid.RunUpDistanceX2 0.299 \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
