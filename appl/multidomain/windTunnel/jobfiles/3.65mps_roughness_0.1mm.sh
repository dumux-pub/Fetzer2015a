#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/3.65mps/roughness/0.1mm
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpen $simdir
cp $source/executables/davarzani2014_plain_3.65mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpen davarzani2014_plain_3.65mps.input \
  -ZeroEq.BBoxMinSandGrainRoughness 0.1e-3 \
  -ZeroEq.EddyViscosityModel 2 \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
