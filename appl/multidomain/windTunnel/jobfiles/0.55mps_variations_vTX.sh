#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/0.55mps/variations/vTX
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopOpen $simdir
cp $source/executables/davarzani2014_plain_0.55mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopOpen davarzani2014_plain_0.55mps.input \
  -FreeFlow.VelDataFile /home/lh2/fetzer/results/windtunnel/davarzani2014/0.55mps_windSpeed.dat \
  -FreeFlow.TemperatureDataFile /home/lh2/fetzer/results/windtunnel/davarzani2014/0.55mps_temperature.dat \
  -FreeFlow.MassFractionDataFile /home/lh2/fetzer/results/windtunnel/davarzani2014/0.55mps_massFraction.dat \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
