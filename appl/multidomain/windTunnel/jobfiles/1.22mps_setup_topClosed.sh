#!/bin/bash
umask 022
source=/temp/fetzer/resultsNupusPaper/
simdir=$source/results/1.22mps/setup/topClosed
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir
cp $source/executables/test_windTunnelTopClosed $simdir
cp $source/executables/davarzani2014_plain_1.22mps.input $simdir
cd $simdir

echo "simulation starts" > logfile.out
./test_windTunnelTopClosed davarzani2014_plain_1.22mps.input \
  -Grid.NumberOfCellsY 67 \
  -Grid.RefineTopY true \
  -Grid.UpperRightX 1.25 \
  -Grid.UpperRightY 0.51 \
  -Grid.NoDarcyX1 0.80 \
  -Grid.NoDarcyX2 1.05 \
  -Grid.RunUpDistanceX1 0.801 \
  -Grid.RunUpDistanceX2 1.049 \
  >> logfile.out
echo "simulation ended" >> logfile.out
exit 0
