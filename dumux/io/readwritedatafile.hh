// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief This file can be used to read input like experimental data from a
 *        file. The evaluate function can be used to obtain the right values
 *        and setting them as boundary conditions.
 */
#ifndef DUMUX_READ_DATA_FILE_HH
#define DUMUX_READ_DATA_FILE_HH

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <limits>
#include <cmath>

#include <dune/common/debugstream.hh>

namespace Dumux
{

void printData(std::vector<double> data[2])
{
  for (unsigned int i = 0; i < data[0].size(); ++i)
      std::cout << data[0][i] << " " << data[1][i]  << std::endl;
}

void checkData(std::vector<double> data[2], std::string dataName="")
{
  double min[2] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::max()};
  double max[2] = {std::numeric_limits<double>::min(), std::numeric_limits<double>::min()};
  double sum = 0.0;
  for (unsigned int i = 0; i < data[0].size(); ++i)
  {
    if (data[0][i] > data[0][i+1] && i+1 < data[0].size())
    {
      std::cout << "Error: time data is not increasing between number " << i+1 << " and " << i+2 << "." << std::endl;
      DUNE_THROW(Dune::IOError, "Error, see line before.");
    }

    if (std::isnan(data[1][i]))
    {
      std::cout << "Error: data point " << i+1 << " is not a number." << std::endl;
      DUNE_THROW(Dune::IOError, "Error, see line before.");
    }

    if (std::isinf(data[1][i]))
    {
      std::cout << "Error: data point " << i+1 << " is not a inf." << std::endl;
      DUNE_THROW(Dune::IOError, "Error, see line before.");
    }

    if (data[1][i] < 1e-10 && data[1][i] > -1e-10)
      std::cout << "Warning: data point " << i+1 << " is zero." << std::endl;

    if (data[0][i] > max[0])
      max[0] = data[0][i];

    if (data[0][i] < min[0])
      min[0] = data[0][i];

    if (data[1][i] > max[1])
      max[1] = data[1][i];

    if (data[1][i] < min[1])
      min[1] = data[1][i];

    sum += data[1][i];
  }
  std::cout << dataName
            << " min[0]: " << min[0] << " max[0]: " << max[0]
            << " min[1]: " << min[1] << " max[1]: " << max[1]
            << " mean[1]: " << sum/data[1].size() << " numEntries[1]: " << data[1].size()
            << std::endl;  
}

int readData(std::string filename, std::vector<double> data[2])
{
  std::string line;
  std::ifstream myfile (filename.c_str());
  if (myfile.is_open())
  {
    unsigned int i = 0;
    while (std::getline (myfile, line)) // first line is a comment
    {
      double a, b;
      i++;
      if (myfile >> a >> b)
      {
        data[0].push_back(a);
        data[1].push_back(b);
      }
      else
        std::cout << "Warning: could not pass value number: " << i << std::endl;
    }
    myfile.close();
    checkData(data, filename);
    return 1;
  }
  else
  {
    std::cout << "Unable to open file: " << filename << "." << std::endl;
    DUNE_THROW(Dune::IOError, "Error, see line before.");
  }

  return 0;
}

const double evaluateData(const std::vector<double> data[2],
                          const double startTime,
                          const double endTime,
                          const unsigned int averagingMethod = 3)
{
  if(startTime > endTime)
    DUNE_THROW(Dune::RangeError, "startTime is greater than endTime.");

  int startIdx = -1;
  int endIdx = -1;
  for (unsigned int i = 0; i < data[0].size(); ++i)
  {
    if (data[0][i] > startTime && startIdx == -1)
      startIdx = i;
    if (data[0][i] > endTime && endIdx == -1)
      endIdx = i-1;
  }

  if (startIdx == -1)
    startIdx = data[0].size()-1;
  if (endIdx == -1)
    endIdx = data[0].size()-1;

//   std::cout << "startIdx: " << startIdx << " endIdx: " << endIdx << "  :  " << data[0][startIdx] << " " << data[0][endIdx] << std::endl;
  // exactly one data point in interval
  if (startIdx == endIdx)
    return data[1][startIdx];
  // no value in interval
  if (startIdx > endIdx) //! \todo use interpolation here
    return data[1][startIdx-1];
  // time is large the input data
  if (startIdx == -1 && endIdx == -1)
    return data[1][data[1].size()-1];
  // more than one data point in interval
  if (startIdx < endIdx)
  {
    double sum = 0.0;

    // first value
    if (averagingMethod == 0)
      return data[1][startIdx];

    // last value
    if (averagingMethod == 1)
      return data[1][endIdx];

    // arithmetic averaging
    if (averagingMethod == 2)
    {
      for (unsigned int i = startIdx; i < endIdx+1; ++i)
      {
        sum += data[1][i];
      }
      return sum/(endIdx - startIdx + 1);
    }

    // data (e.g time) weighted averaging
    if (averagingMethod == 3)
    for (unsigned int i = startIdx; i < endIdx+1; ++i)
    {
      double deltaT = 0.0;
      if (i == startIdx)
//         {
        deltaT = (data[0][i+1] + data[0][i]) / 2.0 - startTime;
//       std::cout << data[0][i+1] << " "<< data[0][i] << " " << startTime << std::endl;
//         }
      else if (i == endIdx)
        deltaT = endTime - (data[0][i] + data[0][i-1]) / 2.0;
      else
        deltaT = (data[0][i+1] + data[0][i]) / 2.0 - (data[0][i] + data[0][i-1]) / 2.0;
//       std::cout << data[0][i] << " "<< data[0][i+1] << " " << deltaT  << " "<<  data[1][i] << std::endl;
      sum += data[1][i] * deltaT;
    }
    return sum/(endTime-startTime);
  }

  DUNE_THROW(Dune::Exception, "Unknown evaluation error!");
  return(0);
}

void writeDataFile(std::vector<double> data[2],
                   std::string dataName[2],
                   std::string filename)
{
  std::ofstream myfile (filename.c_str());
  if (myfile.is_open())
  {
    myfile << "#" << dataName[0] << " " << dataName[1] << std::endl;
    for (unsigned int i = 0; i < data[0].size(); ++i)
      myfile << data[0][i] << " " << data[1][i] << std::endl;

    myfile.close();
    checkData(data, filename);
  }
  else
  {
    std::cout << "Unable to open file: " << filename << "." << std::endl;
    DUNE_THROW(Dune::IOError, "Error, see line before.");
  }
}

} // namespace Dumux

#endif // DUMUX_READ_DATA_FILE_HH