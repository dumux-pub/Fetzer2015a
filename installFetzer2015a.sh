#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone git://github.com/smuething/dune-multidomain.git
git clone git://github.com/smuething/dune-multidomaingrid.git
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2015a.git dumux-Fetzer2015a

### Go to specific branches
cd dune-common && git checkout releases/2.3 && cd ..
cd dune-geometry && git checkout releases/2.3 && cd ..
cd dune-grid && git checkout releases/2.3 && cd ..
cd dune-istl && git checkout releases/2.3 && cd ..
cd dune-localfunctions && git checkout releases/2.3 && cd ..
cd dune-multidomain && git checkout releases/2.0 && cd ..
cd dune-multidomaingrid && git checkout releases/2.3 && cd ..
cd dune-pdelab && git checkout releases/2.0 && cd ..
cd dune-typetree && git checkout releases/2.3 && cd ..
cd dumux && git checkout releases/2.8 && cd ..
cd dumux-Fetzer2015a && git checkout v_2 && cd ..

### Go to specific commits
cd dune-common && git checkout 8cfcea54a78fee457993729535b7c63b3497be5e && cd ..
cd dune-geometry && git checkout 8e5075fc4c58e116d4a0f003a4d1f0482a3ab618 && cd ..
cd dune-grid && git checkout 2749fb531fc2ab027ca728f69634478b9ec9b814 && cd ..
cd dune-istl && git checkout b5979486b15ad529251b815fb63a9b4d2c765b49 && cd ..
cd dune-localfunctions && git checkout eedfe2a7639be4a7e6dc457fc5181454088aebe0 && cd ..
cd dune-multidomain && git checkout b38dcc116153d4e39181f984383cefa7afd8139f && cd ..
cd dune-multidomaingrid && git checkout 1cc143d52e6875e120dbc9d20faad50fee1f9ecd && cd ..
cd dune-pdelab && git checkout f7a7ff4a23e6f43967b006318480ffbf894ff63f && cd ..
cd dune-typetree && git checkout ecffa10c59fa61a0071e7c788899464b0268719f && cd ..
cd dumux && git checkout 81eef3e58e36c756381a19f611a701846c765d2e && cd ..
cd dumux-Fetzer2015a && git checkout 7b36b7467d664056992f4cc5a8185de93e7d4c39 && cd ..

### EXTERNAL MODULES: UG is required
./dumux-Fetzer2015a/installexternal.sh ug

### Run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" dumux-Fetzer2015a/gcc-optim.opts > dumux-Fetzer2015a/gcc-optim_used.opts
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" dumux-Fetzer2015a/clang-optim.opts > dumux-Fetzer2015a/clang-optim_used.opts
./dune-common/bin/dunecontrol --opts=dumux-Fetzer2015a/gcc-optim_used.opts all
./dune-common/bin/dunecontrol --opts=dumux-Fetzer2015a/clang-optim_used.opts all
